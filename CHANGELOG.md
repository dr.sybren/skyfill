# Skyfill Changelog

## Version 1.6 (2024-11-21)

- Add `-out sometype` CLI option to control output format, where `sometype` is `auto`, `jpeg`, `png`, or `tiff`.
  It's also possible to pass `-out list` and Skyfill will list the supported formats.
- Copy EXIF, IPTC, and XMP metadata from the input to the output file. Currently only works when reading & writing JPEG files.
- Copy the embedded ICC profile from the input to the output file. As with the metadata, only works when reading & writing JPEG files.
- Write GPano XMP information to the output file. Use the `-no-gpano-xmp` CLI option to disable this. Currently only works when writing JPEG files. See https://developers.google.com/streetview/spherical-metadata for more information.
- Write a thumbnail to JPEG files. This was a necessity to add, as the above-mentioned copying of the EXIF would mean that the thumbnail of the input image would also be copied (and thus would show a non-skyfilled image).
- The Windows executable now has an icon and some metadata (product version, copyright, etc).
- On Windows a message is shown in a popup, when Skyfill is run without arguments. This should explain how the application is intended to use when just double-cicking the executable.
- Add support for reading WebP images.
- Make sky gap detection a bit more loose. Previous versions assumed that the sky gap was one uniform colour (taken from the top left corner). Now it still takes the top left corner as 'sky gap colour', but any colour that is somewhat similar will be considered part of the sky gap.  This non-exact matching is done so that you can do colour grading (and applying things like 'film-like' looks that might not uniformly map colours) on a drone panorama, and run Skyfill afterwards.

## Version 1.5 (2021-07-25)

- Improve performance by using a lookup table for colour linearisation.
- Add `-mirrorball` CLI option to fill the sky with a mirror-ball reflecting the
  ground, instead of the default blurring behaviour.

## Version 1.4 (2021-03-31)

- Add `-quality` CLI option to control the JPEG quality of the output file.
  Previous versions always used 90. Values from 0 to 100 can be used.

## Version 1.3 (2020-10-13)

- Fixed bug where some 16-bit files would be written without file extension.


## Version 1.2 (2020-09-13)

- Fixed bug where too much of the sky was filled up.
- Add CLI option `-adobergb` to indicate the input should be interpreted as
  AdobeRGB. Otherwise sRGB is assumed.
- Write 16-bit TIFF when the input is 16-bit.


## Version 1.1 (2019-05-06)

- Assume full white is also not part of the sky and should be filled.
- Save to both PNG and JPEG.


## Version 1.0 (2019-03-17)

- First released version
