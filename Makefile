VERSION := $(shell git describe --tags --dirty)
PACKAGE_PATH := dist/skyfill-${VERSION}

BUILDTOOL := mage
ifeq ($(OS),Windows_NT)
	BUILDTOOL := $(BUILDTOOL).exe
endif
BUILDTOOL_PATH := ${PWD}/${BUILDTOOL}

ifndef PACKAGE_PATH
# ${PACKAGE_PATH} is used in 'rm' commands, so it's important to check.
$(error PACKAGE_PATH is not set)
endif

skyfill: buildtool
	"${BUILDTOOL_PATH}" skyfill

gpanoxmp: buildtool
	"${BUILDTOOL_PATH}" gpanoxmp

# Builds the buildtool itself, for faster rebuilds of Skyfill.
buildtool: ${BUILDTOOL}
${BUILDTOOL}: mage.go $(wildcard magefiles/*.go) go.mod
	go run mage.go -compile "${BUILDTOOL_PATH}"

profile:
	go test -bench=. -cpuprofile cpu.prof ./filler
	go tool pprof -http=:8080 cpu.prof

version:
	@echo "OS     : ${OS}"
	@echo "Version: ${VERSION}"

test: buildtool
	"${BUILDTOOL_PATH}" test

benchmark: buildtool
	"${BUILDTOOL_PATH}" benchmark

check: buildtool
	"${BUILDTOOL_PATH}" check

clean:
	go clean -i -x

.gitlabAccessToken:
	$(error gitlabAccessToken does not exist, visit Visit https://gitlab.com/-/user_settings/personal_access_tokens, create a Personal Access Token with API access then save it to the file .gitlabAccessToken)

release: .gitlabAccessToken buildtool package
	rsync ${PACKAGE_PATH}* stuvelfoto@stuvel.eu:downloads/skyfill/ -va
	"${BUILDTOOL_PATH}" release ${PACKAGE_PATH}\*

package:
	@$(MAKE) _prepare_package
	@$(MAKE) _package_linux
	@$(MAKE) _package_windows
	@$(MAKE) _package_darwin
	@$(MAKE) _finish_package

package_linux:
	@$(MAKE) _prepare_package
	@$(MAKE) _package_linux
	@$(MAKE) _finish_package

package_windows:
	@$(MAKE) _prepare_package
	@$(MAKE) _package_windows
	@$(MAKE) _finish_package

package_darwin:
	@$(MAKE) _prepare_package
	@$(MAKE) _package_darwin
	@$(MAKE) _finish_package

_package_linux:
	@$(MAKE) --no-print-directory GOOS=linux GOARCH=amd64 BINARY_OUT=${PACKAGE_PATH}/skyfill _package_tar

_package_windows:
	@$(MAKE) --no-print-directory GOOS=windows GOARCH=amd64 BINARY_OUT=${PACKAGE_PATH}/skyfill.exe _package_zip

_package_darwin:
	@$(MAKE) --no-print-directory GOOS=darwin GOARCH=amd64 BINARY_OUT=${PACKAGE_PATH}/skyfill _package_zip

_prepare_package: vet lint
	rm -rf "${PACKAGE_PATH}"
	mkdir -p "${PACKAGE_PATH}"
	cp -ua README.md LICENSE demo ${PACKAGE_PATH}/

_finish_package:
	rm -r "${PACKAGE_PATH}"
	rm -f "${PACKAGE_PATH}.sha256"
	sha256sum ${PACKAGE_PATH}* | tee ${PACKAGE_PATH}.sha256

_package_tar: skyfill
	tar -C $(dir ${PACKAGE_PATH}) -zcf $(PWD)/${PACKAGE_PATH}-${GOOS}.tar.gz $(notdir ${PACKAGE_PATH})
	rm "${BINARY_OUT}"

_package_zip: skyfill
	"${BUILDTOOL_PATH}" zipdir ${PACKAGE_PATH}-${GOOS}.zip ${PACKAGE_PATH}
	rm "${BINARY_OUT}"

.PHONY: run skyfill gpanoxmp version buildtool vet lint deploy package release profile
