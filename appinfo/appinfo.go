package appinfo

// SPDX-License-Identifier: GPL-3.0-or-later

import (
	"fmt"
	"runtime/debug"
)

// ApplicationName contains the application name.
const ApplicationName = "Skyfill"

const ApplicationPackage = "gitlab.com/dr.sybren/skyfill"

// ApplicationVersion is the version number of the application.
// It is set during the build.
var ApplicationVersion = "set-during-build"

// ApplicationGitHash has the Git hash of the commit used to create this build.
var ApplicationGitHash string

func init() {
	ApplicationGitHash = "-unknown-"

	buildInfo, ok := debug.ReadBuildInfo()
	if !ok {
		return
	}
	for _, setting := range buildInfo.Settings {
		if setting.Key == "vcs.revision" {
			ApplicationGitHash = setting.Value
			return
		}
	}
}

// FormattedApplicationInfo returns the application name & version as single string.
func FormattedApplicationInfo() string {
	return fmt.Sprintf("%s %s", ApplicationName, ApplicationVersion)
}
