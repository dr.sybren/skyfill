package binsearch

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2019 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"gitlab.com/dr.sybren/skyfill/lincolor"
)

func Search(pixels []lincolor.Pixel, searchX int) (result int) {
	mid := len(pixels) / 2
	switch {
	case len(pixels) == 0:
		result = -1 // not found
	case pixels[mid].X > searchX:
		result = Search(pixels[:mid], searchX)
	case pixels[mid].X < searchX:
		result = Search(pixels[mid+1:], searchX)
		result += mid + 1
	default: // pixels[mid].X == searchX
		result = mid // found
	}
	return
}

func Interpolate(pixels []lincolor.Pixel, searchX, imageWidth int) lincolor.LinearColor {
	if len(pixels) == 1 {
		return pixels[0].Color
	}

	var found, next lincolor.Pixel

	index := Search(pixels, searchX) // the index of the biggest X <= searchX
	if index == -1 {
		// This means the pixel is before the first one.
		lastIndex := len(pixels) - 1
		found = pixels[lastIndex]
		found.X -= imageWidth
		next = pixels[0]
	} else {
		found = pixels[index]
		if found.X == searchX {
			return found.Color
		}
		next = pixels[(index+1)%len(pixels)]
	}

	nextX := next.X
	if nextX <= found.X {
		// Wrapping the edge of the image.
		nextX += imageWidth
	}
	return lincolor.Interpolate(
		found.Color,
		next.Color,
		float64(nextX-searchX),
		float64(searchX-found.X),
	)
}
