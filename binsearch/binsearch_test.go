package binsearch

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2019 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/dr.sybren/skyfill/lincolor"
)

func Test_Search(t *testing.T) {
	pixels := []lincolor.Pixel{
		lincolor.Pixel{X: 1, Color: lincolor.LinearColor{R: 0.0, G: 0.0, B: 0.0}},
		lincolor.Pixel{X: 5, Color: lincolor.LinearColor{R: 1.0, G: 1.0, B: 1.0}},
		lincolor.Pixel{X: 8, Color: lincolor.LinearColor{R: 0.5, G: 0.4, B: 0.2}},
	}
	assert.Equal(t, -1, Search(pixels, 0))
	assert.Equal(t, 0, Search(pixels, 1))
	assert.Equal(t, 0, Search(pixels, 2))
	assert.Equal(t, 1, Search(pixels, 5))
	assert.Equal(t, 1, Search(pixels, 6))
	assert.Equal(t, 1, Search(pixels, 7))
	assert.Equal(t, 2, Search(pixels, 8))
	assert.Equal(t, 2, Search(pixels, 10))
}

func Test_Interpolate(t *testing.T) {
	pixels := []lincolor.Pixel{
		lincolor.Pixel{X: 1, Color: lincolor.LinearColor{R: 0.0, G: 0.0, B: 0.0}},
		lincolor.Pixel{X: 5, Color: lincolor.LinearColor{R: 1.0, G: 1.0, B: 1.0}},
		lincolor.Pixel{X: 8, Color: lincolor.LinearColor{R: 0.5, G: 0.4, B: 0.2}},
	}
	imageWidth := 10
	assert.InDelta(t, 0.16666666666666666, Interpolate(pixels, 0, imageWidth).R, 0.000001)
	assert.InDelta(t, 0.0, Interpolate(pixels, 1, imageWidth).R, 0.000001)
	assert.InDelta(t, 0.25, Interpolate(pixels, 2, imageWidth).R, 0.000001)
	assert.InDelta(t, 1.0, Interpolate(pixels, 5, imageWidth).R, 0.000001)
	assert.InDelta(t, 0.8333333333333333, Interpolate(pixels, 6, imageWidth).R, 0.000001)
	assert.InDelta(t, 0.6666666666666667, Interpolate(pixels, 7, imageWidth).R, 0.000001)
	assert.InDelta(t, 0.5, Interpolate(pixels, 8, imageWidth).R, 0.000001)
	assert.InDelta(t, 0.3333333333333333, Interpolate(pixels, 9, imageWidth).R, 0.000001)
}
