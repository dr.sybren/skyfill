package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/dr.sybren/skyfill/imgio"
)

func main() {
	parseCliArgs()
	configLogging()

	logger := logrus.WithField("filename", cliArgs.filename)

	img := imgio.LoadImage(cliArgs.filename)
	if err := img.AddGPanoXMP(); err != nil {
		logger.WithError(err).Fatal("could not add GPano XMP data")
	}

	imgio.WriteImage(cliArgs.filename, img, 90)

	logger.Info("Done!")
}

var cliArgs struct {
	filename string
	quiet    bool
	debug    bool
}

func parseCliArgs() {
	flag.BoolVar(&cliArgs.quiet, "quiet", false, "Disable info-level logging.")
	flag.BoolVar(&cliArgs.debug, "debug", false, "Enable debug-level logging.")

	flag.Parse()

	if flag.NArg() != 1 {
		flag.Usage()
		fmt.Println("  filename")
		fmt.Println("        Input file; JPEG only")
		os.Exit(1)
	}

	cliArgs.filename = flag.Arg(0)
	if cliArgs.filename == "" {
		logrus.Fatal("empty filename not allowed")
	}
}

func configLogging() {
	logrus.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
	})

	// Only log the warning severity or above by default.
	var level logrus.Level
	switch {
	case cliArgs.debug:
		level = logrus.DebugLevel
	case cliArgs.quiet:
		level = logrus.WarnLevel
	default:
		level = logrus.InfoLevel
	}
	logrus.SetLevel(level)
	log.SetOutput(logrus.StandardLogger().Writer())
}
