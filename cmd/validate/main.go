package main

import (
	"io"
	"os"

	jpegstructure "github.com/dsoprea/go-jpeg-image-structure/v2"
	"github.com/sirupsen/logrus"
)

func main() {
	filename := os.Args[1]

	logger := logrus.WithField("filename", filename)
	logger.Info("loading image")

	fileReader, err := os.Open(filename)
	if err != nil {
		logger.WithError(err).Fatal("unable to open file")
	}
	defer fileReader.Close()

	imageBytes, err := io.ReadAll(fileReader)
	if err != nil {
		logger.WithError(err).Fatal("unable to read file")
	}

	jmp := jpegstructure.NewJpegMediaParser()
	intfc, parseErr := jmp.ParseBytes(imageBytes)
	switch {
	case intfc == nil:
		logger.WithError(parseErr).Fatal("error parsing input image JPEG data")
	case parseErr != nil:
		logger.WithError(parseErr).Warn("error parsing input image JPEG data, but some data could be obtained")
	}

	segments := intfc.(*jpegstructure.SegmentList)
	if err := segments.Validate(imageBytes); err != nil {
		logger.WithError(err).Fatal("JPEG structure error")
	}

	logger.Info("JPEG structure OK")
}
