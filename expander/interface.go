package expander

import (
	"gitlab.com/dr.sybren/skyfill/lincolor"
	"gitlab.com/dr.sybren/skyfill/sky"
)

type ImageExpander interface {
	Setup(sourceImage lincolor.Image, skySize sky.Size)
	Expand(targetImage lincolor.Image)
	DrawSamples(targetImage lincolor.Image)
}
