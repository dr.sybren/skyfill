package filler

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"fmt"
	"image"
	"image/color"

	"github.com/sirupsen/logrus"
	"gitlab.com/dr.sybren/skyfill/expander"
	"gitlab.com/dr.sybren/skyfill/imgio"
	"gitlab.com/dr.sybren/skyfill/lincolor"
	"gitlab.com/dr.sybren/skyfill/sky"
	"gitlab.com/dr.sybren/skyfill/timer"
)

// Skyfiller is the main entry point for Skyfill operations.
type Skyfiller struct {
	BlendAreaFactor    float64 // Height of the blend area, in percent (100%=1.0) of detected sky gap height.
	SkyHeight          int     // Height of the sky gap in pixels. Non-zero overrides automatic sky gap detection.
	LowerSkyFactor     float64 // Amount to lower the sky boundary by, in percent (100%=1.0) of detected sky gap height.
	ShowSamples        bool    // Show sample points as red pixels.
	ShowBounds         bool    // Show sky gap bounds.
	AdobeRGB           bool    // Assume AdobeRGB instead of sRGB.
	CompressionQuality int     // JPEG compression quality (0-100) of the output file (if JPEG).
}

// New returns a new Skyfiller instance with some sensible default values.
func New() Skyfiller {
	return Skyfiller{
		BlendAreaFactor:    0.10,
		CompressionQuality: 90,
	}
}

func (sf *Skyfiller) determineSky(image image.Image) (skySize sky.Size) {
	var logMessage string

	if sf.SkyHeight > 0 {
		skySize = sky.NewSize(image.Bounds(), sf.SkyHeight, sf.BlendAreaFactor)
		logMessage = "sky height given/known"
	} else {
		skySize = sky.FindSkyHeight(image, sf.BlendAreaFactor, sf.LowerSkyFactor)
		logMessage = "detected sky"
	}

	logrus.WithFields(logrus.Fields{
		"pureSkyHeight":   skySize.PureSkyHeight,
		"blendAreaPixels": skySize.BlendHeight,
	}).Info(logMessage)

	if skySize.IsEmpty() {
		logrus.Fatal("no sky detected, nothing to fill")
	}

	return
}

func (sf *Skyfiller) findTransferFunction(sourceImage image.Image) lincolor.TransferFunction {
	// TODO(Sybren): Get this from the image metadata rather than requiring CLI arguments.
	if sf.AdobeRGB {
		return lincolor.TransferFunctionAdobeRGB{}
	}
	return lincolor.TransferFunctionSRGB{}
}

func (sf *Skyfiller) LoadImage(filename string) imgio.WithMetadata {
	imgWithMeta := imgio.LoadImage(filename)

	if len(imgWithMeta.Metadata) > 0 {
		logrus.WithField("metadata", imgWithMeta.MetadataTypes()).Debug("loaded image metadata")
	}

	return imgWithMeta
}

// Process skyfills a single image.
func (sf *Skyfiller) Process(inputImage imgio.Mutable, expander expander.ImageExpander) {
	tf := sf.findTransferFunction(inputImage)
	logrus.WithFields(logrus.Fields{
		"colorSpace": tf.Name(),
		"colorModel": fmt.Sprintf("%T", inputImage.ColorModel().Convert(color.RGBA{})),
	}).Info("color info")

	sourceImage := lincolor.WrapImage(inputImage, tf)
	skySize := sf.determineSky(sourceImage)

	sf.SkyHeight = skySize.PureSkyHeight // Store for any following iteration of the skyfiller.

	skyImage := lincolor.WrapImage(
		imgio.NewImage(sourceImage.ColorModel(), skySize.TotalBounds()),
		tf)

	logrus.Info("filling sky")
	fillTracker := timer.NewDurationTracker()
	expander.Setup(sourceImage, skySize)
	expander.Expand(skyImage)

	fillTracker.Logger("fillDuration").Info("blending sky")
	blendTracker := timer.NewDurationTracker()
	sky.Blend(sourceImage, skyImage, skySize)

	if sf.ShowSamples {
		expander.DrawSamples(sourceImage)
	}

	if sf.ShowBounds {
		skySize.DrawBounds(sourceImage)
	}
	blendTracker.Logger("blendDuration").Info("blending done")
}

func (sf *Skyfiller) SaveImage(outputFilename string, image imgio.WithMetadata) (err error) {
	logrus.WithField("filename", outputFilename).Info("writing image")
	imgio.WriteImage(outputFilename, image, sf.CompressionQuality)
	return err
}
