package filler

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"image"
	"os"
	"testing"

	// Import for side-effect of registering decoder.
	"image/color"
	_ "image/png"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/dr.sybren/skyfill/imgio"
	"gitlab.com/dr.sybren/skyfill/pixeltriangle"
)

const (
	testingFilename             = "testdata/skyfill-demo-source.png"
	testingOutputFilename       = "testdata/skyfill-demo-source-filled.png"
	testingExpectResultFilename = "testdata/skyfill-demo-source-expect.png"
	testingDiffFilename         = "testdata/skyfill-demo-source-diff.png"
)

// This file is not part of the Skyfill sources, as it's not shared with a
// Creative Commons (or compatible) license.
const benchmarkFilename = "testdata/skyfill-benchmark.tif"

func BenchmarkFiller(b *testing.B) {
	_, err := os.Stat(benchmarkFilename)
	if err != nil {
		b.Skip("benchmark file not found: ", benchmarkFilename)
	}
	logrus.SetLevel(logrus.ErrorLevel)
	skyfiller := New()
	expander := pixeltriangle.New()
	testImage := skyfiller.LoadImage(benchmarkFilename)

	for i := 0; i < b.N; i++ {
		skyfiller.Process(testImage.Image, expander)
	}
}

func TestFiller(t *testing.T) {
	os.Remove(testingDiffFilename)
	logrus.SetLevel(logrus.ErrorLevel)
	skyfiller := New()
	expander := pixeltriangle.NewWithSeed(0x47)

	testImage := skyfiller.LoadImage(testingFilename)
	skyfiller.Process(testImage.Image, expander)

	err := skyfiller.SaveImage(testingOutputFilename, testImage)
	if err != nil {
		t.Errorf("returned error: %v", err)
	}

	outImage := loadImage(t, testingOutputFilename)
	expectImage := loadImage(t, testingExpectResultFilename)
	diffImage := image.NewRGBA(expectImage.Bounds())

	assert.EqualValues(t, expectImage.Bounds(), outImage.Bounds(), "Expected the image to keep its size")

	bounds := expectImage.Bounds()
	var (
		differentPixels  uint64
		maxError         uint16
		maxErrX, maxErrY int
	)
	for y := bounds.Min.Y; y < bounds.Max.Y+1; y++ {
		for x := bounds.Min.X; x < bounds.Max.X+1; x++ {
			expectPixel := expectImage.At(x, y)
			actualPixel := outImage.At(x, y)

			diff, diffColor := pixelDifference(expectPixel, actualPixel)
			diffImage.Set(x, y, diffColor)

			if diff > 0 {
				differentPixels++
				if diff > maxError {
					maxError = diff
					maxErrX = x
					maxErrY = y
				}
			}
		}
	}
	if differentPixels > 0 {
		imgWithMeta := imgio.WithMetadata{Image: diffImage}
		imgio.WriteImage(testingDiffFilename, imgWithMeta, 100)
	} else {
		os.Remove(testingOutputFilename)
		os.Remove(testingDiffFilename)
	}
	assert.Zero(t, differentPixels, "different pixels found, max diff was %v (%v @ 8-bit values) at (%v, %v)", maxError, maxError>>8, maxErrX, maxErrY)
}

func loadImage(t *testing.T, fname string) image.Image {
	fileReader, err := os.Open(fname)
	require.NoError(t, err)
	if fileReader == nil {
		return nil
	}

	defer fileReader.Close()

	image, formatName, err := image.Decode(fileReader)
	require.NoError(t, err)
	assert.Equal(t, "png", formatName)
	return image
}

func pixelDifference(expectPixel, actualPixel color.Color) (uint16, color.Color) {
	abs := func(a uint32, b uint32) uint32 {
		if a > b {
			return a - b
		}
		return b - a
	}

	max := func(values ...uint16) uint16 {
		var max uint16
		for _, value := range values {
			if value > max {
				max = value
			}
		}
		return max
	}

	eR, eG, eB, _ := expectPixel.RGBA()
	aR, aG, aB, _ := actualPixel.RGBA()

	color := color.RGBA64{
		uint16(abs(eR, aR)),
		uint16(abs(eG, aG)),
		uint16(abs(eB, aB)),
		0xffff,
	}
	diff := max(color.R, color.G, color.B)
	return diff, color
}
