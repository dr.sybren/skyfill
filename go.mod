module gitlab.com/dr.sybren/skyfill

require (
	github.com/dsoprea/go-exif/v3 v3.0.0-20210428042052-dca55bf8ca15
	github.com/dsoprea/go-jpeg-image-structure/v2 v2.0.0-20221012074422-4f3f7e934102
	github.com/magefile/mage v1.15.0
	github.com/pkg/browser v0.0.0-20240102092130-5ac0b6a4141c
	github.com/rodrigocfd/windigo v0.0.0-20230809154420-8faa606d9f5f
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.8.4
	github.com/tc-hib/winres v0.2.1
	github.com/trimmer-io/go-xmp v1.0.0
	golang.org/x/image v0.18.0
	golang.org/x/vuln v1.1.3
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce
	honnef.co/go/tools v0.5.1
)

require (
	github.com/BurntSushi/toml v1.4.1-0.20240526193622-a339e1f7089c // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dsoprea/go-iptc v0.0.0-20200609062250-162ae6b44feb // indirect
	github.com/dsoprea/go-logging v0.0.0-20200710184922-b02d349568dd // indirect
	github.com/dsoprea/go-photoshop-info-format v0.0.0-20200609050348-3db9b63b202c // indirect
	github.com/dsoprea/go-utility/v2 v2.0.0-20200717064901-2fccff4aa15e // indirect
	github.com/go-errors/errors v1.1.1 // indirect
	github.com/go-xmlfmt/xmlfmt v0.0.0-20191208150333-d5b6f63a941b // indirect
	github.com/golang/geo v0.0.0-20200319012246-673a6f80352d // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/exp/typeparams v0.0.0-20231108232855-2478ac86f678 // indirect
	golang.org/x/mod v0.19.0 // indirect
	golang.org/x/net v0.27.0 // indirect
	golang.org/x/sync v0.7.0 // indirect
	golang.org/x/sys v0.22.0 // indirect
	golang.org/x/telemetry v0.0.0-20240522233618-39ace7a40ae7 // indirect
	golang.org/x/tools v0.23.0 // indirect
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

go 1.22.5

// Replace staticcheck with my fork so it includes https://github.com/dominikh/go-tools/pull/1597
replace honnef.co/go/tools v0.5.1 => github.com/sybrenstuvel/staticcheck v0.0.0-20240827070457-50574d1110b0
