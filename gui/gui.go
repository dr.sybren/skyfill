// Package gui provides a graphical user interface.
//
// The GUI is only meant (for now) to show a notification how the app should be
// used.

package gui

const (
	helpURL = "https://stuvel.eu/software/skyfill/#using-skyfill"
)

func ShowUsage() {
	showUsage()
}
