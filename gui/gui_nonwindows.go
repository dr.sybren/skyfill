//go:build !windows

package gui

import "fmt"

func showUsage() {
	fmt.Println("no GUI supported on this platform")
	fmt.Println()
	fmt.Println("Visit for more information:", helpURL)
}
