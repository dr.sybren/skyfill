package gui

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"

	"github.com/pkg/browser"
	"github.com/rodrigocfd/windigo/ui"
	"github.com/rodrigocfd/windigo/win"
	"github.com/rodrigocfd/windigo/win/co"
	"gitlab.com/dr.sybren/skyfill/appinfo"
)

func showUsage() {
	runtime.LockOSThread()

	myWindow := NewMyWindow() // instantiate
	myWindow.wnd.RunAsMain()  // ...and run
}

// This struct represents our main window.
type MyWindow struct {
	wnd         ui.WindowMain
	usageLabel  ui.Static
	closeButton ui.Button
	helpButton  ui.Button
}

// Creates a new instance of our main window.
func NewMyWindow() *MyWindow {
	exeName := filepath.Base(os.Args[0])
	helpMessage := fmt.Sprintf(
		"Skyfill does not have a graphical user interface.\n"+
			"Its simplest use: drag an image onto %s\n"+
			"\n"+
			"Run `%s -help` in a terminal to see advanced options\n",
		exeName, exeName)

	const (
		textWidth   = 270
		textHeight  = 16 * 5
		textMarginX = 22
		textMarginY = 16

		buttonX = textMarginX
		buttonY = textHeight + 2*textMarginY
	)

	wnd := ui.NewWindowMain(
		ui.WindowMainOpts().
			Title(appinfo.FormattedApplicationInfo()).
			ClientArea(win.SIZE{Cx: textWidth + 2*textMarginX, Cy: buttonY + 40}),
	)

	me := &MyWindow{
		wnd: wnd,
		usageLabel: ui.NewStatic(wnd,
			ui.StaticOpts().
				Text(helpMessage).
				Position(win.POINT{X: textMarginX, Y: textMarginY}).
				Size(win.SIZE{Cx: textWidth, Cy: textHeight}),
		),
		// First-created button gets keyboard focus.
		closeButton: ui.NewButton(wnd,
			ui.ButtonOpts().
				Text("&Close").
				Position(win.POINT{X: buttonX, Y: buttonY}),
		),
		helpButton: ui.NewButton(wnd,
			ui.ButtonOpts().
				Text("&More Info").
				Position(win.POINT{X: buttonX + 120, Y: buttonY}),
		),
	}

	me.closeButton.On().BnClicked(func() {
		win.PostQuitMessage(1)
	})

	me.helpButton.On().BnClicked(func() {
		err := browser.OpenURL(helpURL)
		if err != nil {
			msg := fmt.Sprintf("Could not open web browser: %v\n%s", err, helpURL)
			me.wnd.Hwnd().MessageBox(msg, "Could not open web browser", co.MB_ICONERROR)
			return
		}
		win.PostQuitMessage(1)
	})
	return me
}
