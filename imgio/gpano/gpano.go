package gpano

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2024 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"fmt"

	"github.com/trimmer-io/go-xmp/xmp"
)

var (
	NsGPano = xmp.NewNamespace("GPano", "http://ns.google.com/photos/1.0/panorama/", NewModel)

	ProjectionEquirectangular = "equirectangular"
)

func init() {
	xmp.Register(NsGPano, xmp.ImageMetadata)
}

type GPano struct {
	CroppedAreaImageHeightPixels int      `xmp:"GPano:CroppedAreaImageHeightPixels,empty"`
	CroppedAreaImageWidthPixels  int      `xmp:"GPano:CroppedAreaImageWidthPixels,empty"`
	CroppedAreaLeftPixels        int      `xmp:"GPano:CroppedAreaLeftPixels,empty"`
	CroppedAreaTopPixels         int      `xmp:"GPano:CroppedAreaTopPixels,empty"`
	FullPanoHeightPixels         int      `xmp:"GPano:FullPanoHeightPixels,empty"`
	FullPanoWidthPixels          int      `xmp:"GPano:FullPanoWidthPixels,empty"`
	ProjectionType               string   `xmp:"GPano:ProjectionType"` // equirectangular
	UsePanoramaViewer            xmp.Bool `xmp:"GPano:UsePanoramaViewer"`
}

func NewModel(name string) xmp.Model {
	return &GPano{}
}

func MakeModel(d *xmp.Document) (*GPano, error) {
	m, err := d.MakeModel(NsGPano)
	if err != nil {
		return nil, err
	}
	return m.(*GPano), nil
}

func FindModel(d *xmp.Document) *GPano {
	if m := d.FindModel(NsGPano); m != nil {
		return m.(*GPano)
	}
	return nil
}

func (m *GPano) Namespaces() xmp.NamespaceList {
	return xmp.NamespaceList{NsGPano}
}

func (m *GPano) Can(nsName string) bool {
	return nsName == NsGPano.GetName()
}

func (m *GPano) SyncModel(d *xmp.Document) error {
	return nil
}

func (m *GPano) SyncFromXMP(d *xmp.Document) error {
	return nil
}

func (m *GPano) SyncToXMP(d *xmp.Document) error {
	return nil
}

func (m *GPano) CanTag(tag string) bool {
	_, err := xmp.GetNativeField(m, tag)
	return err == nil
}

func (m *GPano) GetTag(tag string) (string, error) {
	v, err := xmp.GetNativeField(m, tag)
	if err != nil {
		return "", fmt.Errorf("%s: %w", NsGPano.GetName(), err)
	}
	return v, nil
}

func (x *GPano) SetTag(tag, value string) error {
	if err := xmp.SetNativeField(x, tag, value); err != nil {
		return fmt.Errorf("%s: %w", NsGPano.GetName(), err)
	}
	return nil
}
