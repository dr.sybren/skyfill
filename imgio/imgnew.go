package imgio

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"

	"github.com/sirupsen/logrus"
)

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2019 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

// NewImage constructs a new image.
func NewImage(colorModel color.Model, bounds image.Rectangle) draw.Image {
	targetColor := colorModel.Convert(color.RGBA{})
	switch targetColor.(type) {
	case color.RGBA, color.NRGBA:
		return image.NewNRGBA(bounds)
	case color.RGBA64, color.NRGBA64:
		return image.NewNRGBA64(bounds)
	}

	logger := logrus.WithField("colorModel", fmt.Sprintf("%T", targetColor))
	logger.Fatal("unable to create a new image with this color model")

	return nil
}
