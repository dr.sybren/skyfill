package imgio

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2019 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"bytes"
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"io"
	"os"
	"path"
	"slices"

	jpegstructure "github.com/dsoprea/go-jpeg-image-structure/v2"
	"github.com/sirupsen/logrus"
	"gitlab.com/dr.sybren/skyfill/lincolor"
	"gitlab.com/dr.sybren/skyfill/timer"
	"golang.org/x/image/tiff"
)

// WriteImage writes to an image file. Compression is chosen based on the filename.
func WriteImage(fname string, imgWithMetadata WithMetadata, compressionQuality int) {
	var img image.Image = imgWithMetadata.Image

	// Convert a wrapped image to its wrapped type, so that the encoder can detect
	// the proper bit depth from it.
	if linearImage, ok := img.(lincolor.Image); ok {
		img = linearImage.Wrapped()
	}

	logger := logrus.WithField("file", fname)
	logger.Debug("writing image")
	tracker := timer.NewDurationTracker()

	writer, err := os.Create(fname)
	if err != nil {
		logger.WithError(err).Fatal("unable to open file")
	}
	defer writer.Close()

	ext := path.Ext(fname)
	switch ext {
	case ".png":
		err = png.Encode(writer, img)
	case ".jpg", ".jpeg":
		err = writeWithMetadata(writer, imgWithMetadata, compressionQuality)
	case ".tif", ".tiff":
		err = tiff.Encode(writer, img, &tiff.Options{
			Compression: tiff.Deflate,
			Predictor:   true,
		})
	default:
		logger.WithField("ext", ext).Fatal("unknown extension, unable to write image")
	}
	if err != nil {
		logger.WithError(err).Fatal("unable to write image")
	}

	tracker.LogField(logger, "writeDuration").Info("image written")
}

func writeWithMetadata(writer io.Writer, imgWithMetadata WithMetadata, compressionQuality int) error {
	if len(imgWithMetadata.Metadata) == 0 {
		// Shortcut: just write the JPEG directly, instead of the
		// writing-then-parsing necessary for splicing in the metadata.
		return jpeg.Encode(writer, imgWithMetadata.Image, &jpeg.Options{Quality: compressionQuality})
	}

	if err := imgWithMetadata.AddThumbnail(); err != nil {
		return err
	}

	// Encode to a buffer, so that we can split up the JPEG data again, to insert
	// the metadata segments. Not the most efficient, but the only way I know how
	// to do this.
	buffer := bytes.Buffer{}
	err := jpeg.Encode(
		&buffer,
		imgWithMetadata.Image,
		&jpeg.Options{Quality: compressionQuality},
	)
	if err != nil {
		return err
	}

	// Parse the skyfilled JPEG into segments.
	jmp := jpegstructure.NewJpegMediaParser()
	intfc, parseErr := jmp.ParseBytes(buffer.Bytes())
	switch {
	case parseErr != nil:
		return fmt.Errorf("error parsing output image JPEG data: %w", parseErr)
	case intfc == nil:
		return fmt.Errorf("unknown error parsing output image JPEG data")
	}
	segmentList := intfc.(*jpegstructure.SegmentList)

	// Splice the metadata into the output.
	segments := slices.Insert(segmentList.Segments(), 1, imgWithMetadata.RawSegments()...)

	// Actually write the JPEG segments to the writer.
	segmentListWithMetadata := jpegstructure.NewSegmentList(segments)
	if err := segmentListWithMetadata.Write(writer); err != nil {
		return fmt.Errorf("could not write JPEG to output file: %w", err)
	}

	return nil
}
