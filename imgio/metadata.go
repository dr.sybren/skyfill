package imgio

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2024 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"bytes"
	"fmt"
	"image"
	"image/jpeg"
	"math"

	goexif "github.com/dsoprea/go-exif/v3"
	exifcommon "github.com/dsoprea/go-exif/v3/common"
	jpegstructure "github.com/dsoprea/go-jpeg-image-structure/v2"
	"github.com/sirupsen/logrus"
	"github.com/trimmer-io/go-xmp/xmp"
	"gitlab.com/dr.sybren/skyfill/imgio/gpano"
	"golang.org/x/image/draw"
)

var (
	xmpPrefix  = []byte("http://ns.adobe.com/xap/1.0/\000")
	iccPrefix  = []byte("ICC_PROFILE")
	jfifPrefix = []byte{74, 70, 73, 70, 0} // "JFIF" + 0-byte.
)

const (
	// Thumbnail size in pixels.
	// These should be even numbers, as that's assumed in thumbnailRect() below.
	thumbnailWidth  = 512
	thumbnailHeight = 256
)

// JPEGSegment adds some more functions to jpegstructure.Segment.
// All functions we need from jpegstructure.Segment have to be wrapped on this
// type as well.
type JPEGSegment jpegstructure.Segment

// IsMetadata determines whether a segment is considered 'metadata', and thus
// has to be copied from the input file to the output of Skyfill.
func (s *JPEGSegment) IsMetadata() bool {
	return false ||
		s.IsXmp() ||
		s.IsExif() ||
		s.IsIptc() ||
		s.IsICC() ||
		s.IsJFIF() ||
		false
}

func (s *JPEGSegment) IsXmp() bool {
	return (*jpegstructure.Segment)(s).IsXmp()
}
func (s *JPEGSegment) IsExif() bool {
	return (*jpegstructure.Segment)(s).IsExif()
}
func (s *JPEGSegment) IsIptc() bool {
	return (*jpegstructure.Segment)(s).IsIptc()
}
func (s *JPEGSegment) Exif() (rootIfd *goexif.Ifd, data []byte, err error) {
	return (*jpegstructure.Segment)(s).Exif()
}
func (s *JPEGSegment) SetExif(ib *goexif.IfdBuilder) error {
	return (*jpegstructure.Segment)(s).SetExif(ib)
}

// ICC can be stored in multiple sequential JPEG segments, as the ICC profiles
// can be larger than a single segment allows.
func (s *JPEGSegment) IsICC() bool {
	// Source: https://stackoverflow.com/a/61759129/875379
	if s.MarkerId != jpegstructure.MARKER_APP2 {
		return false
	}

	l := len(iccPrefix)

	if len(s.Data) < l {
		return false
	}

	return bytes.Equal(s.Data[:l], iccPrefix)
}

func (s *JPEGSegment) IsJFIF() bool {
	// Source: https://www.fileformat.info/format/jpeg/egff.htm
	if s.MarkerId != jpegstructure.MARKER_APP0 {
		return false
	}

	l := len(jfifPrefix)

	if len(s.Data) < l {
		return false
	}

	return bytes.Equal(s.Data[:l], jfifPrefix)
}

// WithMetadata contains a mutable image with its JPEG metadata.
// The Metadata field is only non-nil if Format = "jpeg".
type WithMetadata struct {
	Image    Mutable
	Format   string
	Metadata []*JPEGSegment
}

func (wm *WithMetadata) RawSegments() []*jpegstructure.Segment {
	rawSegments := make([]*jpegstructure.Segment, len(wm.Metadata))
	for i, segment := range wm.Metadata {
		rawSegments[i] = (*jpegstructure.Segment)(segment)
	}
	return rawSegments
}

// MetadataTypes returns a slice of "EXIF", "IPTC", etc, depending on
// which metadata has been found. The returned slice has the same length as
// wm.Metadata.
func (wm *WithMetadata) MetadataTypes() []string {
	types := make([]string, len(wm.Metadata))

	for index, segment := range wm.Metadata {
		switch {
		case segment.IsExif():
			types[index] = "EXIF"
		case segment.IsIptc():
			types[index] = "IPTC"
		case segment.IsXmp():
			types[index] = "XMP"
		case segment.IsICC():
			types[index] = "ICC"
		case segment.IsJFIF():
			types[index] = "JFIF"
		default:
			panic(fmt.Sprintf("unknown metadata type on segment %d (%v)", index, segment))
		}
	}

	return types
}

func (wm *WithMetadata) xmpSegment() *JPEGSegment {
	for _, segment := range wm.Metadata {
		if segment.IsXmp() {
			return segment
		}
	}
	return nil
}

func (wm *WithMetadata) HasXMPSegment() bool {
	return wm.xmpSegment() != nil
}

func (wm *WithMetadata) exifSegment() *JPEGSegment {
	for _, segment := range wm.Metadata {
		if segment.IsExif() {
			return segment
		}
	}
	return nil
}

func (wm *WithMetadata) HasEXIFSegment() bool {
	return wm.exifSegment() != nil
}

func (wm *WithMetadata) XMPBytes() []byte {
	segment := wm.xmpSegment()
	if segment == nil {
		return nil
	}
	return segment.Data[len(xmpPrefix):]
}

func (wm *WithMetadata) XMPBytesSet(xmpBytes []byte) {
	xmpSegment := wm.xmpSegment()

	if xmpSegment == nil {
		xmpSegment = &JPEGSegment{
			MarkerId:   jpegstructure.MARKER_APP1,
			MarkerName: "APP1",
			Offset:     0, // Hopefully this will be set when writing the final JPEG.
			Data:       nil,
		}
		wm.Metadata = append(wm.Metadata, xmpSegment)
	}

	xmpSegment.Data = xmpPrefix
	xmpSegment.Data = append(xmpSegment.Data, xmpBytes...)
}

// AddGPanoXMP adds an XMP section indicating that this is a panorama photo.
//   - If there is no XMP metadata section yet, it is added.
//   - If there is XMP already, it is extended.
//   - If there is already a GPano section in there, this function assumes it is
//     correct and doesn't touch it.
func (wm *WithMetadata) AddGPanoXMP() error {
	// Construct an XMP document. This will be filled with the XMP data from the
	// image, if it is available. Otherwise it'll just be empty.
	document := xmp.NewDocument()

	if wm.HasXMPSegment() {
		xmpBytes := wm.XMPBytes()

		if err := xmp.Unmarshal(xmpBytes, document); err != nil {
			return fmt.Errorf("could not parse existing XMP data: %w", err)
		}
	}

	// See if there is a GPano section already. If so, don't do anything and assume it's OK.
	gpanoXMP := document.FindModel(gpano.NsGPano)
	if gpanoXMP != nil {
		logrus.Info("found GPano XMP data, keeping it as-is")
		return nil
	}
	logrus.Debug("XMP has no GPano section, going to add one")

	// Create a new GPano section.
	bounds := wm.Image.Bounds()
	gpanoXMP = &gpano.GPano{
		CroppedAreaImageHeightPixels: bounds.Dy(),
		CroppedAreaImageWidthPixels:  bounds.Dx(),
		CroppedAreaLeftPixels:        0,
		CroppedAreaTopPixels:         0,
		FullPanoHeightPixels:         bounds.Dx(),
		FullPanoWidthPixels:          bounds.Dy(),
		ProjectionType:               gpano.ProjectionEquirectangular,
		UsePanoramaViewer:            true,
	}

	_, err := document.AddModel(gpanoXMP)
	if err != nil {
		return fmt.Errorf("could not add GPano section to XMP: %w", err)
	}

	// Print the XML for fun.
	updatedXMPBytes, err := xmp.Marshal(document)
	if err != nil {
		return fmt.Errorf("could not marshal XMP data back to bytes after adding a GPano section: %w", err)
	}

	// Replace the XMP metadata JPEG section with the new XMP document.
	wm.XMPBytesSet(updatedXMPBytes)
	return nil
}

// AddThumbnail downscales the image to thumbnail size, and stores it as EXIF thumbnail.
func (wm *WithMetadata) AddThumbnail() error {

	// Create an image thumbnail & store as JPEG bytes.
	var thumbBytes []byte
	{
		thumb := thumbnail(wm.Image)

		logrus.WithFields(logrus.Fields{
			"width":  thumb.Bounds().Dx(),
			"height": thumb.Bounds().Dy(),
		}).Info("adding EXIF thumbnail")

		thumbBuffer := bytes.Buffer{}
		err := jpeg.Encode(&thumbBuffer, thumb, &jpeg.Options{Quality: 75})
		if err != nil {
			return fmt.Errorf("could not encode thumbnail as JPEG: %w", err)
		}
		thumbBytes = thumbBuffer.Bytes()
	}

	exifSegment := wm.exifSegment()
	var rootIFDBuilder *goexif.IfdBuilder
	if exifSegment == nil {
		// Create a new EXIF segment + root IFD builder.
		exifSegment = &JPEGSegment{
			MarkerId:   jpegstructure.MARKER_APP1,
			MarkerName: "APP1",
			Offset:     0, // Hopefully this will be set when writing the final JPEG.
			Data:       nil,
		}
		wm.Metadata = append(wm.Metadata, exifSegment)

		im, err := exifcommon.NewIfdMappingWithStandard()
		if err != nil {
			return fmt.Errorf("could not get standard EXIF ID mapping: %w", err)
		}
		rootIFDBuilder = goexif.NewIfdBuilder(
			im,
			goexif.NewTagIndex(),
			exifcommon.IfdStandardIfdIdentity,
			exifcommon.EncodeDefaultByteOrder)
	} else {
		// Parse the existing EXIF to get a root IFD builder.
		rootIFD, _, err := exifSegment.Exif()
		if err != nil {
			return fmt.Errorf("could not parse existing EXIF data to replace/add a thumbnail: %w", err)
		}
		rootIFDBuilder = goexif.NewIfdBuilderFromExistingChain(rootIFD)
	}

	// The thumbnail should be in the IFD _after_ the root one.
	thumbIFDBuilder, err := rootIFDBuilder.NextIb()
	if err != nil {
		// This will never happen, as rootIFDBuilder.NextIb() always returns err=nil.
		return fmt.Errorf("cannot get next IB: %w", err)
	}

	if thumbIFDBuilder == nil {
		// Create the IFDBuilder that will store the thumbnail.
		im, err := exifcommon.NewIfdMappingWithStandard()
		if err != nil {
			return fmt.Errorf("could not get standard EXIF ID mapping: %w", err)
		}

		thumbIFDBuilder = goexif.NewIfdBuilder(
			im,
			goexif.NewTagIndex(),
			exifcommon.IfdStandardIfdIdentity,
			exifcommon.EncodeDefaultByteOrder)

		err = rootIFDBuilder.SetNextIb(thumbIFDBuilder)
		if err != nil {
			return fmt.Errorf("cannot set next-after-root IFD: %w", err)
		}
	}

	// Set the thumbnail data.
	err = thumbIFDBuilder.SetThumbnail(thumbBytes)
	if err != nil {
		return fmt.Errorf("cannot set thumbnail on EXIF IFD %s: %w", thumbIFDBuilder.String(), err)
	}

	// Write the EXIF data back to the metadata segment.
	if err := exifSegment.SetExif(rootIFDBuilder); err != nil {
		return fmt.Errorf("could not set new EXIF data with thumbnail: %w", err)
	}

	return nil
}

func thumbnailRect(sourceRect image.Rectangle) image.Rectangle {
	if sourceRect.Min != image.Pt(0, 0) {
		panic("this code can only handle image rectangles that start at (0, 0)")
	}

	sourceSize := sourceRect.Size()
	if sourceSize == (image.Point{}) {
		return image.Rectangle{}
	}

	// Don't scale up to create a thumbnail.
	if sourceSize.X <= thumbnailWidth && sourceSize.Y <= thumbnailHeight {
		return sourceRect
	}

	scaleX := thumbnailWidth / float64(sourceSize.X)
	scaleY := thumbnailHeight / float64(sourceSize.Y)

	if scaleX < scaleY {
		height := math.RoundToEven(scaleX * float64(sourceSize.Y))
		return image.Rect(0, 0, thumbnailWidth, int(math.Max(height, 1)))
	}

	width := math.RoundToEven(scaleY * float64(sourceSize.X))
	return image.Rect(0, 0, int(math.Max(width, 1)), thumbnailHeight)
}

func thumbnail(sourceImage image.Image) image.Image {
	sourceRect := sourceImage.Bounds()
	thumbRect := thumbnailRect(sourceRect)

	if thumbRect == sourceRect {
		return sourceImage
	}

	thumb := image.NewRGBA(thumbRect)

	// CatmullRom produces really nice, smooth results, but is ~2x slower than
	// BiLinear. BiLinear is certainly acceptable.
	interpolator := draw.BiLinear
	interpolator.Scale(thumb, thumbRect, sourceImage, sourceRect, draw.Src, nil)

	return thumb
}
