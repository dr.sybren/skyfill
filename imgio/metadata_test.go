package imgio

import (
	"image"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestThumbnailRect(t *testing.T) {
	assert.Equal(t,
		image.Rect(0, 0, 512, 256),
		thumbnailRect(image.Rect(0, 0, 512, 256)),
		"identity",
	)
	assert.Equal(t,
		image.Rect(0, 0, 512, 256),
		thumbnailRect(image.Rect(0, 0, 564, 282)),
		"approx 1.1x bigger",
	)
	assert.Equal(t,
		image.Rect(0, 0, 512, 256),
		thumbnailRect(image.Rect(0, 0, 2048, 1024)),
		"4x bigger",
	)
	assert.Equal(t,
		image.Rect(0, 0, 512, 256),
		thumbnailRect(image.Rect(0, 0, 2048, 1024)),
		"4x bigger",
	)

	assert.Equal(t,
		image.Rect(0, 0, 512, 128),
		thumbnailRect(image.Rect(0, 0, 1024, 256)),
		"aspect > 2",
	)
	assert.Equal(t,
		image.Rect(0, 0, 384, 256),
		thumbnailRect(image.Rect(0, 0, 768, 512)),
		"aspect < 2",
	)
	assert.Equal(t,
		image.Rect(0, 0, 256, 256),
		thumbnailRect(image.Rect(0, 0, 500, 500)),
		"square",
	)
	assert.Equal(t,
		image.Rect(0, 0, 0, 0),
		thumbnailRect(image.Rect(0, 0, 0, 0)),
		"empty",
	)
	assert.Equal(t,
		image.Rect(0, 0, 20, 10),
		thumbnailRect(image.Rect(0, 0, 20, 10)),
		"tiny",
	)
	assert.Equal(t,
		image.Rect(0, 0, 512, 1),
		thumbnailRect(image.Rect(0, 0, 8192, 1)),
		"thin and wide",
	)
	assert.Equal(t,
		image.Rect(0, 0, 1, 256),
		thumbnailRect(image.Rect(0, 0, 1, 4096)),
		"thin and high",
	)
}
