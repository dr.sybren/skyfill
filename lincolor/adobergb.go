package lincolor

import (
	"image/color"
	"math"

	"gitlab.com/dr.sybren/skyfill/lookuptable"
)

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

// TransferFunctionAdobeRGB converts between linear and AdobeRGB.
type TransferFunctionAdobeRGB struct {
}

// Name returns "AdobeRGB".
func (tf TransferFunctionAdobeRGB) Name() string {
	return "AdobeRGB"
}

// ToLinear converts the color from AdobeRGB to linear.
func (tf TransferFunctionAdobeRGB) ToLinear(c color.Color) LinearColor {
	return simpleToLinear(c, adobeRGB2linearLookup)
}

// FromLinear returns the linear color as AdobeRGB.
func (tf TransferFunctionAdobeRGB) FromLinear(lc LinearColor) color.Color {
	return simpleFromLinear(lc, linear2AdobeRGBLookup)
}

var linear2adobeRGBLookupTable lookuptable.LookupTable
var adobeRGB2linearLookupTable lookuptable.LookupTable

func init() {
	// A table size of 8192 produces (in the sRGB test case) exactly the same results as doing the math, but faster.
	adobeRGB2linearLookupTable = lookuptable.New(8192, adobeRGB2linear)
	linear2adobeRGBLookupTable = lookuptable.New(8192, linear2AdobeRGB)
}

// Converts float [0, 1] to float [0, 1] but with sRGB to linear gamma conversion.
func adobeRGB2linearLookup(argb float64) float64 {
	return adobeRGB2linearLookupTable.Lookup(argb)
}

// Converts float [0, 1] to float [0, 1] but with linear to sRGB gamma conversion.
func linear2AdobeRGBLookup(linear float64) float64 {
	return linear2adobeRGBLookupTable.Lookup(linear)
}

// Converts float [0, 1] to float [0, 1] but with AdobeRGB to linear gamma conversion.
func adobeRGB2linear(argb float64) float64 {
	return math.Pow(argb, 2.19921875)
}

// Converts float [0, 1] to float [0, 1] but with linear to AdobeRGB gamma conversion.
func linear2AdobeRGB(linear float64) float64 {
	return math.Pow(linear, 1/2.19921875)
}
