package lincolor

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2019 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"image/color"
	"testing"

	"github.com/stretchr/testify/assert"
)

func assertSameColor(t *testing.T, colorA, colorB color.Color) {
	rA, gA, bA, aA := colorA.RGBA()
	rB, gB, bB, aB := colorB.RGBA()

	assert.Equal(t, int(rA), int(rB), "Red channel")
	assert.Equal(t, int(gA), int(gB), "Green channel")
	assert.Equal(t, int(bA), int(bB), "Blue channel")
	assert.Equal(t, int(aA), int(aB), "Alpha channel")
}

func assertSameLinear(t *testing.T, colorA, colorB LinearColor) {
	assert.InDelta(t, colorA.R, colorB.R, 1e-5, "Red channel")
	assert.InDelta(t, colorA.G, colorB.G, 1e-5, "Green channel")
	assert.InDelta(t, colorA.B, colorB.B, 1e-5, "Blue channel")
}
