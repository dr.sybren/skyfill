package lincolor

import (
	"image"
	"image/color"
	"image/draw"
)

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

// Image wraps an image to allow operations in linear color space.
type Image struct {
	wrapped      image.Image
	transferFunc TransferFunction
}

var _ image.Image = (*Image)(nil)

// WrapImage wraps an existing image with a transfer function.
func WrapImage(img image.Image, transferFunc TransferFunction) Image {
	return Image{img, transferFunc}
}

// Wrapped returns the wrapped image.
func (i Image) Wrapped() image.Image {
	return i.wrapped
}

// ColorModel returns the Image's color model.
func (i Image) ColorModel() color.Model {
	return i.wrapped.ColorModel()
}

// Bounds returns the domain for which At can return non-zero color.
// The bounds do not necessarily contain the point (0, 0).
func (i Image) Bounds() image.Rectangle {
	return i.wrapped.Bounds()
}

// At returns the color of the pixel at (x, y) in the image color space.
// At(Bounds().Min.X, Bounds().Min.Y) returns the upper-left pixel of the grid.
// At(Bounds().Max.X-1, Bounds().Max.Y-1) returns the lower-right one.
func (i Image) At(x, y int) color.Color {
	return i.wrapped.At(x, y)
}

// LinearAt returns the color of the pixel at (x, y) in linear color.
// At(Bounds().Min.X, Bounds().Min.Y) returns the upper-left pixel of the grid.
// At(Bounds().Max.X-1, Bounds().Max.Y-1) returns the lower-right one.
func (i Image) LinearAt(x, y int) LinearColor {
	color := i.At(x, y)
	return i.transferFunc.ToLinear(color)
}

// Set changes a single pixel value to the color in image color space.
func (i Image) Set(x, y int, c color.Color) {
	mutable, ok := i.wrapped.(draw.Image)
	if !ok {
		panic("image is not mutable, does not implement draw.Image")
	}
	mutable.Set(x, y, c)
}

// LinearSet changes a single pixel value to the color in linear color.
func (i Image) LinearSet(x, y int, lc LinearColor) {
	mutable, ok := i.wrapped.(draw.Image)
	if !ok {
		panic("image is not mutable, does not implement draw.Image")
	}
	mutable.Set(x, y, i.transferFunc.FromLinear(lc))
}

// AverageColor returns the average pixel color of a horizontal line of pixels.
func (i Image) AverageColor(line, startX, nrOfPixels int) LinearColor {
	sum := LinearColor{}
	imgWidth := i.Bounds().Dx()

	var pixel LinearColor
	for dx := 0; dx < nrOfPixels; dx++ {
		pixel = i.LinearAt((startX+dx+imgWidth)%imgWidth, line)
		sum = sum.Plus(pixel)
	}

	return sum.Div(float64(nrOfPixels))
}
