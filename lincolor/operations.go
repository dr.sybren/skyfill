package lincolor

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2019 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

// Plus returns this colour + other
func (lc LinearColor) Plus(linOther LinearColor) LinearColor {
	return LinearColor{
		lc.R + linOther.R,
		lc.G + linOther.G,
		lc.B + linOther.B,
	}
}

// Div returns this colour / factor
func (lc LinearColor) Div(factor float64) LinearColor {
	return LinearColor{
		lc.R / factor,
		lc.G / factor,
		lc.B / factor,
	}
}

// Interpolate returns the linear interpolation of the two colours.
func Interpolate(
	colorA, colorB LinearColor,
	weightA, weightB float64,
) LinearColor {
	alpha := weightB / (weightA + weightB)
	return Alpha(colorA, colorB, alpha)
}

// Alpha returns the alpha blend of the two colours.
func Alpha(
	colorA, colorB LinearColor,
	alpha float64,
) LinearColor {
	blendR := (1-alpha)*colorA.R + alpha*colorB.R
	blendG := (1-alpha)*colorA.G + alpha*colorB.G
	blendB := (1-alpha)*colorA.B + alpha*colorB.B

	return LinearColor{blendR, blendG, blendB}
}
