package lincolor

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2019 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"image/color"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPlus(t *testing.T) {
	tf := TransferFunctionSRGB{}

	colorA := LinearColor{1.0, 0.5, 0.3}
	colorB := tf.ToLinear(color.RGBA{255, 30, 1, 255}) // LinearColor{R:1, G:0.01298303234217301, B:0.0003035269835488375}

	result := colorA.Plus(colorB)
	expect := LinearColor{
		2.0,
		0.51298303234217301,
		0.3003035269835488375,
	}
	assertSameLinear(t, expect, result)
}

func TestDiv(t *testing.T) {
	color := LinearColor{1.0, 0.5, 0.3}
	result := color.Div(0.33333333333)
	expect := LinearColor{3.0, 1.5, 0.9}
	assertSameLinear(t, expect, result)
}

func TestInterpolate(t *testing.T) {
	result := Interpolate(
		LinearColor{1.0, 0.5, 0.3},
		LinearColor{0.1, 0.2, 3.2},
		10,
		0,
	)
	assert.InDelta(t, 1.0, result.R, 0.0001)
	assert.InDelta(t, 0.5, result.G, 0.0001)
	assert.InDelta(t, 0.3, result.B, 0.0001)

	result = Interpolate(
		LinearColor{1.0, 0.5, 0.3},
		LinearColor{0.1, 0.2, 3.2},
		0,
		3,
	)
	assert.InDelta(t, 0.1, result.R, 0.0001)
	assert.InDelta(t, 0.2, result.G, 0.0001)
	assert.InDelta(t, 3.2, result.B, 0.0001)

	result = Interpolate(
		LinearColor{1.0, 0.5, 0.3},
		LinearColor{0.1, 0.2, 3.2},
		8,
		4,
	)
	assert.InDelta(t, 0.7, result.R, 0.0001)
	assert.InDelta(t, 0.4, result.G, 0.0001)
	assert.InDelta(t, 1.2666666, result.B, 0.0001)
}

func TestInterpolateMixedColorModels(t *testing.T) {
	srgb := TransferFunctionSRGB{}

	result := Interpolate(
		srgb.ToLinear(color.RGBA{255, 188, 149, 255}), // which is roughly LinearColor{1.0, 0.5, 0.3}
		LinearColor{0.1, 0.2, 0.7999},
		0,
		3,
	)
	assertSameLinear(t, LinearColor{0.1, 0.2, 0.7999}, result)

	result = Interpolate(
		srgb.ToLinear(color.RGBA{255, 188, 149, 255}), // which is roughly LinearColor{1.0, 0.5, 0.3}
		LinearColor{0.1, 0.2, 0.7999},
		2,
		4,
	)
	assertSameLinear(t, LinearColor{0.4, 0.30096, 0.63344}, result)
}

func TestAlpha(t *testing.T) {
	result := Alpha(
		LinearColor{1.0, 0.5, 0.3},
		LinearColor{0.1, 0.2, 3.2},
		0.3333333,
	)
	assertSameLinear(t, LinearColor{0.7, 0.4, 1.26667}, result)
}

func TestAlphaMixedColorModels(t *testing.T) {
	srgb := TransferFunctionSRGB{}

	// This should be 65535, 48192, 38261, 65535 in 16-bit RGBA.
	colorA := LinearColor{1.0, 0.5, 0.3}

	// lincolor.LinearColor{R:1, G:0.01298303234217301, B:0.0003035269835488375}:
	colorB := color.RGBA{255, 30, 1, 255}

	result := Alpha(colorA, srgb.ToLinear(colorB), 0.33333333)
	assertSameLinear(t, LinearColor{1.0, 0.3376610107807243, 0.20010117566118293}, result)
}
