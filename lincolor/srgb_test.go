package lincolor

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2019 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"image/color"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSRGBFromRGBA(t *testing.T) {
	tf := TransferFunctionSRGB{}

	lincolor := tf.ToLinear(color.RGBA{123, 10, 0, 255})
	assert.InDelta(t, 0.198069319559, lincolor.R, 0.000001)
	assert.InDelta(t, 0.003035269835, lincolor.G, 0.000001)
	assert.Equal(t, 0.0, lincolor.B)

	lincolor = LinearColor{1.0, 0.5, 0.0}
	srgb := tf.FromLinear(lincolor)
	r, g, b, a := srgb.RGBA()
	assert.Equal(t, 65535, int(r))
	assert.Equal(t, 48192, int(g))
	assert.Equal(t, 0, int(b))
	assert.Equal(t, 65535, int(a))
}

func TestSRGBFromRGBA64(t *testing.T) {
	tf := TransferFunctionSRGB{}

	lincolor := tf.ToLinear(color.RGBA64{47127, 2620, 0, 65535})
	assert.InDelta(t, 0.4756890071363063, lincolor.R, 0.000001)
	assert.InDelta(t, 0.0030938292415555, lincolor.G, 0.000001)
	assert.Equal(t, 0.0, lincolor.B)
}

func TestSRGBSymmetry(t *testing.T) {
	tf := TransferFunctionSRGB{}

	original := color.RGBA64{47127, 1, 0, 65535}
	lincolor := tf.ToLinear(original)
	converted := tf.FromLinear(lincolor)

	assertSameColor(t, original, converted)
}
