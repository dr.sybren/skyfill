package lincolor

import (
	"image/color"
	"math"
)

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

// TransferFunction describes how to convert from a colour space to linear color and back.
type TransferFunction interface {
	ToLinear(color color.Color) LinearColor
	FromLinear(color LinearColor) color.Color

	// Name returns the name of the transfer function.
	Name() string
}

// The 'simple' functions make it easier to implement transfer functions.
type simpleTransferFunction func(value float64) float64

func simpleToLinear(c color.Color, toLinearFunc simpleTransferFunction) LinearColor {
	r, g, b, _ := c.RGBA()
	return LinearColor{
		toLinearFunc(u16ToFloat(r)),
		toLinearFunc(u16ToFloat(g)),
		toLinearFunc(u16ToFloat(b)),
	}
}

func simpleFromLinear(lc LinearColor, fromLinearFunc simpleTransferFunction) color.Color {
	return color.RGBA64{
		floatToU16(fromLinearFunc(lc.R)),
		floatToU16(fromLinearFunc(lc.G)),
		floatToU16(fromLinearFunc(lc.B)),
		65535,
	}
}

// Converts float [0, 1] to uint16 [0, 65535].
func floatToU16(zeroToOne float64) uint16 {
	return uint16(math.Round(zeroToOne * 65535.0))
}

// Converts uint [0, 65535] to float [0, 1].
func u16ToFloat(zeroTo16bit uint32) float64 {
	return float64(zeroTo16bit) / 65535.0
}
