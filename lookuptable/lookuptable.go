package lookuptable

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

// Float64Func maps the range [0-1] to [0-1]
type Float64Func func(float64) float64

type LookupTable struct {
	table []float64
}

func New(tableSize int, function Float64Func) LookupTable {
	lt := LookupTable{
		make([]float64, tableSize),
	}
	lt.init(function)
	return lt
}

func (lt *LookupTable) init(function Float64Func) {
	divisor := float64(len(lt.table)) - 1
	for index := range lt.table {
		lt.table[index] = function(float64(index) / divisor)
	}
}

func (lt *LookupTable) Lookup(value float64) float64 {
	if value < 0 {
		return lt.table[0]
	}
	if value >= 1.0 {
		return lt.table[len(lt.table)-1]
	}

	index := value * float64(len(lt.table)-1)
	floor := int(index)
	partial := index - float64(floor)
	return (1-partial)*lt.table[floor] + partial*lt.table[floor+1]
}
