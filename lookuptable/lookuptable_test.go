package lookuptable

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func divTwo(value float64) float64 {
	return value / 2
}

func TestTinyLookupTable(t *testing.T) {
	lt := New(3, divTwo)
	assert.Equal(t, 0.0, lt.Lookup(0))
	assert.Equal(t, 0.1, lt.Lookup(0.2))
	assert.Equal(t, 0.125, lt.Lookup(0.25))
	assert.Equal(t, 0.25, lt.Lookup(0.5))
	assert.Equal(t, 0.45, lt.Lookup(0.9))
	assert.Equal(t, 0.5, lt.Lookup(1), "table: %v", lt.table)
	assert.Equal(t, 0.5, lt.Lookup(1.5), "too-high input value should clamp to highest output value")
	assert.Equal(t, 0.0, lt.Lookup(-1.5), "too-low input value should clamp to lowest output value")
}
