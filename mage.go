//go:build ignore
// +build ignore

// Run with 'go run mage.go' to build Skyfill.

package main

import (
	"os"

	"github.com/magefile/mage/mage"
)

func main() {
	os.Exit(mage.Main())
}
