//go:build mage

package main

import (
	"fmt"
	"image"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"

	"github.com/magefile/mage/sh"
	"github.com/tc-hib/winres"
	"github.com/tc-hib/winres/version"
	"gitlab.com/dr.sybren/skyfill/appinfo"
)

// Build Skyfill.
func Skyfill() error {
	version, err := gitDescribeVersion()
	if err != nil {
		return err
	}
	targetExePath := getBuildTarget("skyfill")

	err = goBuild(".", targetExePath, version)
	if err != nil {
		return err
	}
	return runWinres(targetExePath, version)
}

func Gpanoxmp() error {
	version, err := gitDescribeVersion()
	if err != nil {
		return err
	}
	targetExePath := getBuildTarget("gpanoxmp")

	err = goBuild("./cmd/gpanoxmp", targetExePath, version)
	if err != nil {
		return err
	}
	return runWinres(targetExePath, version)
}

func goBuild(mainPackagePath, targetExePath, appVersion string) error {
	ldflags := fmt.Sprintf("-ldflags=-X %s/appinfo.ApplicationVersion=%s",
		appinfo.ApplicationPackage, appVersion)
	return sh.RunV("go", "build", "-v", ldflags, "-o", targetExePath, mainPackagePath)
}

func gitDescribeVersion() (string, error) {
	return sh.Output("git", "describe", "--tags", "--dirty")
}

func getGOOS() string {
	if goos := os.Getenv("GOOS"); goos != "" {
		return goos
	}
	return runtime.GOOS
}

func getBuildTarget(defaultName string) string {
	target := defaultName

	if binaryOut := os.Getenv("BINARY_OUT"); binaryOut != "" {
		target = binaryOut
	}

	// On Windows we should ensure the path ends in ".exe".
	if getGOOS() == "windows" {
		extension := filepath.Ext(target)
		if extension != ".exe" {
			target += ".exe"
		}
	}

	return target
}

func runWinres(exePath string, version string) error {
	if getGOOS() != "windows" {
		return nil
	}

	in, err := os.Open(exePath)
	if err != nil {
		return fmt.Errorf("could not open input file: %w", err)
	}
	defer in.Close()

	isSigned, err := winres.IsSignedEXE(in)
	if err != nil {
		return fmt.Errorf("unable to determine whether this is a signed exe file: %w", err)
	}
	if isSigned {
		fmt.Printf("executable (%s) is signed, the signature will become invalid\n", exePath)
	}

	rs, err := winres.LoadFromEXE(in)
	if err != nil && err != winres.ErrNoResources {
		return fmt.Errorf("could not get resources from exe file: %w", err)
	}

	err = importIcon(rs)
	if err != nil {
		return fmt.Errorf("could not import resources: %w", err)
	}

	setVersionInfo(rs, version)

	out, err := os.Create(exePath + ".tmp")
	if err != nil {
		return fmt.Errorf("creating output file: %w", err)
	}
	defer out.Close()

	err = rs.WriteToEXE(out, in, winres.WithAuthenticode(winres.IgnoreSignature))
	if err != nil {
		return fmt.Errorf("writing executable to temporary location: %w", err)
	}

	in.Close()
	err = out.Close()
	if err != nil {
		return fmt.Errorf("closing output file: %w", err)
	}

	err = os.Remove(exePath)
	if err != nil {
		return fmt.Errorf("removing existing executable: %w", err)
	}

	err = os.Rename(exePath+".tmp", exePath)
	if err != nil {
		return fmt.Errorf("renaming newly-written executable: %w", err)
	}

	return nil
}

func importIcon(rs *winres.ResourceSet) error {
	filename := "skyfill-icon.png"

	abspath, err := filepath.Abs(filename)
	if err != nil {
		panic(err)
	}

	imgFile, err := os.Open(abspath)
	if err != nil {
		return fmt.Errorf("cannot open icon image file %s: %w", abspath, err)
	}
	defer imgFile.Close()

	img, _, err := image.Decode(imgFile)
	if err != nil {
		return fmt.Errorf("decoding icon image file %s: %w", abspath, err)
	}

	winIcon, err := winres.NewIconFromResizedImage(img, nil)
	if err != nil {
		return fmt.Errorf("creating EXE icon: %w", err)
	}

	err = rs.SetIcon(winres.Name("APPICON"), winIcon)
	if err != nil {
		return fmt.Errorf("setting EXE app icon: %w", err)
	}

	return nil
}

func setVersionInfo(rs *winres.ResourceSet, appVersion string) {
	vi := version.Info{}
	vi.SetFileVersion(appVersion)
	vi.SetProductVersion(appVersion)

	// 'appVersion' is in the form '{tag}-{commits_since_tag}-....'. The code
	// below puts the 'commits_since_tag' part in the last field of the file
	// version.
	appVersionParts := strings.SplitN(appVersion, "-", 3)
	if len(appVersionParts) >= 2 {
		commitsSinceTag, err := strconv.ParseInt(appVersionParts[1], 10, 16)
		if err != nil {
			fmt.Printf("Could not parse version from Git (%s): %v", appVersion, err)
		} else {
			if commitsSinceTag > 65535 {
				panic("HOW many commits since the last tag?")
			}
			vi.FileVersion[3] = uint16(commitsSinceTag)
		}
	}

	// vi.Set() only returns an error if the value contains a zero byte or is
	// empty, none of which is the case here.

	// _ = vi.Set(version.LangNeutral, version.Comments, "")
	_ = vi.Set(version.LangNeutral, version.CompanyName, "dr. Sybren")
	_ = vi.Set(version.LangNeutral, version.FileDescription, "https://stuvel.eu/software/skyfill")
	// _ = vi.Set(version.LangNeutral, version.InternalName, "")
	_ = vi.Set(version.LangNeutral, version.LegalCopyright, "dr. Sybren A. Stüvel")
	// _ = vi.Set(version.LangNeutral, version.LegalTrademarks, "")
	// _ = vi.Set(version.LangNeutral, version.OriginalFilename, "")
	// _ = vi.Set(version.LangNeutral, version.PrivateBuild, "")
	_ = vi.Set(version.LangNeutral, version.ProductName, "Skyfill")
	// _ = vi.Set(version.LangNeutral, version.SpecialBuild, "")

	rs.SetVersionInfo(vi)
}
