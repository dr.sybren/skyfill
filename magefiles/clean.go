//go:build mage

package main

import (
	"fmt"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

func Clean() error {
	args := []string{"clean"}
	if mg.Verbose() {
		args = append(args, "-x")
	}

	if err := sh.RunV(mg.GoCmd(), args...); err != nil {
		return err
	}
	if err := rm(
		"gpanoxmp", "gpanoxmp.exe",
	); err != nil {
		return err
	}
	return nil
}

func rm(path ...string) error {
	for _, p := range path {
		if mg.Verbose() {
			fmt.Println("rm", p)
		}
		if err := sh.Rm(p); err != nil {
			return err
		}
	}
	return nil
}
