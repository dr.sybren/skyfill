//go:build mage

package main

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	"gopkg.in/mgo.v2/bson"

	"github.com/sirupsen/logrus"
)

const (
	gitlabURL       = "https://gitlab.com/api/v4/projects/dr.sybren%2Fskyfill/releases"
	downloadHostURL = "https://stuvelfoto.nl/downloads/skyfill"
)

type gitlabRelease struct {
	Name        string `json:"name"`
	TagName     string `json:"tag_name"`
	Description string `json:"description"`
	Assets      struct {
		Links []gitlabLink `json:"links"`
	} `json:"assets"`
}

type gitlabLink struct {
	Name string `json:"name"`
	URL  string `json:"url"`
}

func makeRequestPayload(version, fileglob string) ([]byte, error) {
	fmt.Println("constructing GitLab release JSON")
	fmt.Println("  version: ", version)
	fmt.Println("  fileglob:", fileglob)

	release := gitlabRelease{
		Name:        version,
		TagName:     version,
		Description: fmt.Sprintf("Version %s of Skyfill", version),
	}

	paths, err := filepath.Glob(fileglob)
	if err != nil {
		return nil, fmt.Errorf("unable to glob %q: %w", fileglob, err)
	}
	if len(paths) == 0 {
		return nil, fmt.Errorf("glob %q matched no files", fileglob)
	}
	for _, fpath := range paths {
		fname := filepath.Base(fpath)
		fmt.Println("      -", fpath)

		link := gitlabLink{
			Name: fname,
			URL:  path.Join(downloadHostURL, fname),
		}
		release.Assets.Links = append(release.Assets.Links, link)
	}

	bytes, err := json.MarshalIndent(release, "", "  ")
	if err != nil {
		return nil, fmt.Errorf("unable to marshal to JSON: %w", err)
	}

	return bytes, nil
}

func loadPersonalAccessToken() (string, error) {
	fname, err := filepath.Abs(".gitlabAccessToken")
	if err != nil {
		return "", fmt.Errorf("unable to construct absolute path: %w", err)
	}
	fmt.Println("reading Gitlab access token from", fname)

	tokenBytes, err := os.ReadFile(fname)
	if err != nil {
		return "", errors.New("unable to read personal access token, see https://gitlab.com/profile/personal_access_tokens")
	}

	token := strings.TrimSpace(string(tokenBytes))
	return token, nil
}

func doGitlabRequest(payload []byte, authToken string) error {
	client := http.Client{
		Timeout: 1 * time.Minute,
	}

	logger := logrus.WithField("url", gitlabURL)
	req, err := http.NewRequest("POST", gitlabURL, bytes.NewReader(payload))
	if err != nil {
		return fmt.Errorf("unable to create HTTP request: %w", err)
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Private-Token", authToken)

	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("performing HTTP request: %w", err)
	}

	logger = logger.WithField("statusCode", resp.StatusCode)
	if resp.Header.Get("Content-Type") != "application/json" {
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			return fmt.Errorf("reading HTTP response: %w", err)
		}

		logger.WithField("body", string(body)).Fatal("non-JSON response from Gitlab")
	}

	response := bson.M{}
	unmarshaller := json.NewDecoder(resp.Body)
	if err := unmarshaller.Decode(&response); err != nil {
		return fmt.Errorf("reading/parsing HTTP response: %w", err)
	}

	niceResponse, err := json.MarshalIndent(response, "", "    ")
	if err != nil {
		return fmt.Errorf("non-JSON response from Gitlab: %v#v", response)
	}

	logger.Info("Response from Gitlab:")
	os.Stdout.Write(niceResponse)

	return nil
}

// Publish a release at Gitlab.
// This assumes that the release package files have been built & uploaded to the
// file hosting service. Reads the Gitlab access token from `.gitlabAccessToken`.
func Release(fileglob string) error {
	version, err := gitDescribeVersion()
	if err != nil {
		return err
	}

	bytes, err := makeRequestPayload(version, fileglob)
	if err != nil {
		return err
	}
	fmt.Println(string(bytes))

	token, err := loadPersonalAccessToken()
	if err != nil {
		return err
	}

	err = doGitlabRequest(bytes, token)
	if err != nil {
		return err
	}

	return nil
}
