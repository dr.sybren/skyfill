//go:build mage

package main

import (
	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"

	"gitlab.com/dr.sybren/skyfill/appinfo"
)

// Run all unit tests.
func Test() error {
	return sh.RunV(mg.GoCmd(), "test", appinfo.ApplicationPackage+"/...")
}

// Run benchmarks.
func Benchmark() error {
	return sh.RunV(mg.GoCmd(), "test", "-bench=.", appinfo.ApplicationPackage+"/...")
}
