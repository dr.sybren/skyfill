package main

// SPDX-License-Identifier: GPL-3.0-or-later

import (
	"archive/zip"
	"compress/flate"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
)

// Zipdir creates a ZIP file from a directory, for Skyfill download packages.
func Zipdir(outputPath, inputPath string) error {
	outfile, err := filepath.Abs(outputPath)
	if err != nil {
		return errors.New("unable make output file path absolute")
	}

	// Open the output file.
	fmt.Println("creating ZIP file", outfile)
	zipFile, err := os.Create(outfile)
	if err != nil {
		return fmt.Errorf("error creating output file: %w", err)
	}
	defer zipFile.Close()
	zipWriter := zip.NewWriter(zipFile)

	zipWriter.RegisterCompressor(zip.Deflate, func(out io.Writer) (io.WriteCloser, error) {
		return flate.NewWriter(out, flate.BestCompression)
	})

	// Copy all the files into the ZIP.
	addToZip := func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			fmt.Printf("error received from filepath.WalkDir, aborting: %v", err)
			return err
		}

		// Construct the path inside the ZIP file.
		relpath, err := filepath.Rel(inputPath, path)
		if err != nil {
			return fmt.Errorf("making %s relative to %s: %w", path, inputPath, err)
		}

		if d.IsDir() {
			switch {
			// case filepath.Base(path) == "__pycache__":
			// 	return fs.SkipDir
			// case relpath == filepath.Join("flamenco", "manager", "docs"):
			// 	return fs.SkipDir
			case strings.HasPrefix(filepath.Base(path), "."):
				// Skip directories like .mypy_cache, etc.
				return fs.SkipDir
			default:
				// Just recurse into this directory.
				return nil
			}
		}

		// Read the file's contents.
		contents, err := os.ReadFile(path)
		if err != nil {
			return fmt.Errorf("reading %s: %w", path, err)
		}

		// Write into the ZIP file.
		fileInZip, err := zipWriter.Create(relpath)
		if err != nil {
			return fmt.Errorf("creating %s in ZIP: %w", relpath, err)
		}
		_, err = fileInZip.Write(contents)
		if err != nil {
			return fmt.Errorf("writing to %s in ZIP: %w", relpath, err)
		}

		return nil
	}

	if err := filepath.WalkDir(inputPath, addToZip); err != nil {
		return fmt.Errorf("error filling ZIP file: %w", err)
	}

	// TODO: insert Skyfill info, like the version number.
	// comment := fmt.Sprintf("%s add-on for Blender, version %s",
	// 	appinfo.ApplicationName,
	// 	appinfo.ApplicationVersion,
	// )
	// if err := zipWriter.SetComment(comment); err != nil {
	// 	return fmt.Errorf("error setting ZIP comment: %w", err)
	// }

	if err := zipWriter.Close(); err != nil {
		return fmt.Errorf("error closing ZIP file: %w", err)
	}

	return nil
}
