package main

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2019 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/dr.sybren/skyfill/appinfo"
	"gitlab.com/dr.sybren/skyfill/filler"
	"gitlab.com/dr.sybren/skyfill/gui"
	"gitlab.com/dr.sybren/skyfill/mirrorball"
	"gitlab.com/dr.sybren/skyfill/outputname"
	"gitlab.com/dr.sybren/skyfill/pixeltriangle"
	"gitlab.com/dr.sybren/skyfill/timer"
)

var cliArgs struct {
	filename       string
	quiet          bool
	debug          bool
	blendArea      int
	lowerSky       int
	mirrorBall     bool
	noGPanoXMP     bool
	outputFileType string
	version        bool
}

var skyfiller filler.Skyfiller

func parseCliArgs() {
	flag.BoolVar(&cliArgs.quiet, "quiet", false, "Disable info-level logging.")
	flag.BoolVar(&cliArgs.debug, "debug", false, "Enable debug-level logging.")
	flag.BoolVar(&cliArgs.version, "version", false, "Show the version of Skyfill and exit.")
	flag.BoolVar(&skyfiller.ShowSamples, "samples", false, "Show sample points as red pixels.")
	flag.BoolVar(&skyfiller.ShowBounds, "bounds", false, "Show sky gap bounds.")
	flag.IntVar(&skyfiller.SkyHeight, "sky", 0, "Height of the sky gap in pixels. Overrides automatic sky gap detection.")
	flag.IntVar(&cliArgs.lowerSky, "lower-sky", 0, "Amount to lower the sky boundary by, in percent of detected sky gap height.")
	flag.IntVar(&cliArgs.blendArea, "blend", 10, "Height of the blend area, in percent of detected sky gap height. Prevents a hard edge between the generated and the real sky.")
	flag.BoolVar(&skyfiller.AdobeRGB, "adobergb", false, "Assume the input image is AdobeRGB. Without this option, sRGB is assumed.")
	flag.BoolVar(&cliArgs.mirrorBall, "mirrorball", false, "Fill the sky using a mirror ball instead of blurring.")
	flag.BoolVar(&cliArgs.noGPanoXMP, "no-gpano-xmp", false, "By default Skyfill will assume the photo is a panorama and write GPano XMP metadata. Use this option to prevent this.")
	flag.IntVar(&skyfiller.CompressionQuality, "quality", 90, "JPEG compression quality (0-100) of the output file (if JPEG).")
	flag.StringVar(&cliArgs.outputFileType, "out", "auto", "Output file type, 'auto' makes Skyfill pick the most suitable to prevent data loss.")

	flag.Parse()

	if cliArgs.version {
		fmt.Printf("%s version %s\n", appinfo.ApplicationName, appinfo.ApplicationVersion)
		fmt.Printf("Git hash: %s\n", appinfo.ApplicationGitHash)
		os.Exit(0)
	}

	// Check -out before the filename, such that '-out list' can produce useful info.
	cliArgs.outputFileType = strings.ToLower(cliArgs.outputFileType)
	if !outputname.IsSupported(cliArgs.outputFileType) {
		logrus.WithFields(logrus.Fields{
			"supported": outputname.SupportedTypes(),
			"requested": cliArgs.outputFileType,
		}).Fatal("unknown output type")
	}

	if flag.NArg() != 1 {
		flag.Usage()
		fmt.Println("  filename")
		fmt.Println("        Input file; JPEG, PNG or TIFF file in 8- or 16-bit RGB or RGBA mode")

		gui.ShowUsage()

		os.Exit(1)
	}

	cliArgs.filename = flag.Arg(0)
	if cliArgs.filename == "" {
		logrus.Fatal("empty filename not allowed")
	}
	skyfiller.BlendAreaFactor = float64(cliArgs.blendArea) / 100.0
	skyfiller.LowerSkyFactor = float64(cliArgs.lowerSky) / 100.0
}

func configLogging() {
	logrus.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
	})

	// Only log the warning severity or above by default.
	var level logrus.Level
	switch {
	case cliArgs.debug:
		level = logrus.DebugLevel
	case cliArgs.quiet:
		level = logrus.WarnLevel
	default:
		level = logrus.InfoLevel
	}
	logrus.SetLevel(level)
	log.SetOutput(logrus.StandardLogger().Writer())
}

func main() {
	totalTracker := timer.NewDurationTracker()

	parseCliArgs()
	configLogging()
	logrus.WithField("version", appinfo.ApplicationVersion).Infof("starting %s", appinfo.ApplicationName)

	image := skyfiller.LoadImage(cliArgs.filename)

	blurringExpander := pixeltriangle.New()
	skyfiller.Process(image.Image, blurringExpander)

	if cliArgs.mirrorBall {
		// Merge the mirrorball into the blurred sky, for a better integration into
		// the image (compared to only doing the mirrorball).
		skyfiller.BlendAreaFactor = 1.0
		skyfiller.SkyHeight /= 2

		mirrorBallExpander := mirrorball.New()
		skyfiller.Process(image.Image, mirrorBallExpander)
	}

	if !cliArgs.noGPanoXMP {
		err := image.AddGPanoXMP()
		if err != nil {
			logrus.WithError(err).Error("could not add GPano XMP, saving image without it")
		}
	}

	outfname := outputname.Filename(cliArgs.filename, cliArgs.outputFileType, image.Image)
	err := skyfiller.SaveImage(outfname, image)

	totalTracker.Logger("totalDuration").Info("done")

	if err != nil {
		logrus.WithError(err).Fatal("unable to skyfill image")
	}
}
