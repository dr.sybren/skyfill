package mirrorball

import (
	"image"
	"image/color"

	"golang.org/x/image/draw"
	"golang.org/x/image/math/f64"

	"gitlab.com/dr.sybren/skyfill/expander"
	"gitlab.com/dr.sybren/skyfill/lincolor"
	"gitlab.com/dr.sybren/skyfill/sky"
)

// Ensure the MirrorBallExpander adheres to the ImageExpander interface.
var _ expander.ImageExpander = (*MirrorBallExpander)(nil)

type MirrorBallExpander struct {
	sourceImage lincolor.Image

	skyRect    image.Rectangle // Part of the image that'll be filled by the mirrorball.
	sourceRect image.Rectangle // Part of the image that'll be copied onto the mirrorball.

	interpolator draw.Interpolator
}

func New() *MirrorBallExpander {
	return &MirrorBallExpander{
		// ApproxBiLinear produces unsatisfactory results.
		// CatmullRom produces really nice, smooth results, but is ~2x slower than BiLinear.
		// BiLinear is certainly acceptable.
		interpolator: draw.BiLinear,
	}
}

// New constructs a pixel triangle suitable for filling up the sky.
func (mb *MirrorBallExpander) Setup(sourceImage lincolor.Image, skySize sky.Size) {
	mb.sourceImage = sourceImage
	mb.skyRect = image.Rect(0, 0, sourceImage.Bounds().Max.X, skySize.TotalHeight())
	mb.sourceRect = image.Rect(0, skySize.TotalHeight(), sourceImage.Bounds().Max.X, sourceImage.Bounds().Max.Y)
}

func (mb *MirrorBallExpander) Expand(targetImage lincolor.Image) {
	// Compute factor to downscale by.
	downscale := float64(mb.skyRect.Dy()) / float64(mb.sourceRect.Dy())

	// Draw flipped onto target image.
	flipY := f64.Aff3{
		1, 0, 0,
		0, -downscale, float64(mb.skyRect.Max.Y) * (1 + downscale),
	}
	mb.interpolator.Transform(targetImage, flipY, mb.sourceImage, mb.sourceRect, draw.Over, nil)
}

func (mb *MirrorBallExpander) DrawSamples(targetImage lincolor.Image) {
	mb.drawLines(targetImage, mb.skyRect, color.RGBA{128, 128, 255, 255}, 0.0)
	mb.drawLines(targetImage, mb.sourceRect, color.RGBA{128, 255, 128, 255}, 0.25)
}

func (mb *MirrorBallExpander) drawLines(targetImage lincolor.Image, drawRect image.Rectangle, drawColor color.Color, xRelativeOffset float64) {
	const (
		lineWidth   = 10
		lineSpacing = 100
	)
	xAbsoluteOffset := int(xRelativeOffset * lineSpacing)

	lineRect := drawRect
	sourceImage := &image.Uniform{drawColor}
	for x := mb.skyRect.Min.X; x <= mb.skyRect.Max.X; x += lineSpacing {
		lineRect.Min.X = x + xAbsoluteOffset
		lineRect.Max.X = x + xAbsoluteOffset + lineWidth
		draw.Draw(targetImage, lineRect, sourceImage, image.Point{}, draw.Over)
	}
}
