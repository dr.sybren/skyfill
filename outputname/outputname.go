package outputname

import (
	"image"
	"image/color"
	"path"
	"strings"
)

// Check -out before the filename, such that '-out list' can produce useful info.
var supportedTypes = map[string]string{
	"auto": "",
	"jpeg": ".jpg",
	"tiff": ".tif",
	"png":  ".png",
}

func IsSupported(outputFileType string) bool {
	_, found := supportedTypes[outputFileType]
	return found
}

func SupportedTypes() []string {
	keyArray := make([]string, 0)
	for key := range supportedTypes {
		keyArray = append(keyArray, key)
	}
	return keyArray
}

func Filename(inputFilename, outputFileType string, outputImage image.Image) string {
	extension := strings.ToLower(path.Ext(inputFilename))
	withoutExtension := inputFilename[:len(inputFilename)-len(extension)]
	outputExtension := suitableExtension(outputFileType, outputImage)
	return withoutExtension + "-filled" + outputExtension
}

func suitableExtension(outputFileType string, outputImage image.Image) string {
	if outputFileType == "auto" {
		switch outputImage.ColorModel() {
		case color.NRGBA64Model, color.RGBA64Model:
			return ".tif"
		default:
			return ".jpg"
		}
	}

	return supportedTypes[outputFileType]
}
