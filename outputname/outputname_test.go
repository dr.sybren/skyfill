package outputname

import (
	"image"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestIsSupported(t *testing.T) {
	assert.False(t, IsSupported("pfff"))
	assert.False(t, IsSupported("JPEG"))
	assert.False(t, IsSupported(""))
	assert.True(t, IsSupported("jpeg"))
	assert.True(t, IsSupported("auto"))
}

func TestFilename(t *testing.T) {
	assert.Equal(t, "input-filled.jpg", Filename("input.jpg", "jpeg", nil))
	assert.Equal(t, "input-filled.tif", Filename("input.jpg", "tiff", nil))
	assert.Equal(t, "input-filled.png", Filename("input.jpg", "png", nil))
}

func TestFilenameAuto(t *testing.T) {
	logrus.SetFormatter(&logrus.TextFormatter{})

	assert.Equal(t, "input-filled.jpg", Filename("input.jpg", "auto", image.NewRGBA(image.Rect(0, 0, 1, 1))))
	assert.Equal(t, "input-filled.jpg", Filename("input.tif", "auto", image.NewRGBA(image.Rect(0, 0, 1, 1))))
	assert.Equal(t, "input-filled.jpg", Filename("input.png", "auto", image.NewRGBA(image.Rect(0, 0, 1, 1))))
	assert.Equal(t, "input-filled.jpg", Filename("input.png", "auto", image.NewNRGBA(image.Rect(0, 0, 1, 1))))
	assert.Equal(t, "input-filled.tif", Filename("input.png", "auto", image.NewRGBA64(image.Rect(0, 0, 1, 1))))
	assert.Equal(t, "input-filled.tif", Filename("input.png", "auto", image.NewNRGBA64(image.Rect(0, 0, 1, 1))))
}
