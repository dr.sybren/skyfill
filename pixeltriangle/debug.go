package pixeltriangle

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"image/color"

	"gitlab.com/dr.sybren/skyfill/lincolor"
)

// DrawSamples draws red pixels in the image at the locations of the pixels in the triangle.
func (pt *PixelTriangle) DrawSamples(targetImage lincolor.Image) {
	red := color.NRGBA{R: 255, G: 0, B: 0, A: 255}
	for line := range pt.pixels {
		for _, pixel := range pt.pixels[line] {
			targetImage.Set(pixel.X, line, red)
		}
	}
}
