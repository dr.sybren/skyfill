package pixeltriangle

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"gitlab.com/dr.sybren/skyfill/lincolor"
)

// Expand overwrites the top part of the target image with the pixel triangle.
// Every row of the image will be overwritten, blending the space between pixels
// in the triangle.
func (pt *PixelTriangle) Expand(targetImage lincolor.Image) {
	imgWidth := targetImage.Bounds().Dx()

	for line, pixels := range pt.pixels {
		// Draw the line of pixels onto the sky image.
		var nextX, nextPixelIndex int
		var nextColor, interCol lincolor.LinearColor
		for pixelIndex, pixel := range pixels {
			targetImage.LinearSet(pixel.X, line, pixel.Color)

			// Interpolate between current and next pixel
			nextPixelIndex = (pixelIndex + 1) % len(pixels)
			nextX = pixels[nextPixelIndex].X
			nextColor = pixels[nextPixelIndex].Color
			if nextX <= pixel.X {
				// Wrapping the edge of the image.
				nextX += imgWidth
			}
			for interX := pixel.X; interX < nextX; interX++ {
				interCol = lincolor.Interpolate(
					pixel.Color,
					nextColor,
					float64(nextX-interX),
					float64(interX-pixel.X),
				)
				targetImage.LinearSet(interX%imgWidth, line, interCol)
			}
		}
	}
}
