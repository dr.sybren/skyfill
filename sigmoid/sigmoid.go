package sigmoid

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2019 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import "math"

// Sigmoid is an auto-scaling auto-shifting sigmoid so that Value(0)=0 and Value(1)=1.
type Sigmoid struct {
	shift float64
	scale float64
}

// NewSigmoid returns a new Sigmoid.
func NewSigmoid() Sigmoid {
	shift := Sigmoid{0.0, 1.0}.Value(0.0)
	scale := Sigmoid{shift, 1.0}.Value(1.0)

	return Sigmoid{shift, 1.0 / scale}
}

// Value returns a value in the [0, 1] interval (assuming x is in [0, 1]).
func (s Sigmoid) Value(x float64) float64 {
	x = (x - 0.5) * 8

	// This is the standard logistic function.
	sigmoid := 1 / (1 + math.Exp(x))

	return ((1 - sigmoid) - s.shift) * s.scale
}
