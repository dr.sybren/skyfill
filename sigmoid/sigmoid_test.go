package sigmoid

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSigmoid(t *testing.T) {
	s := NewSigmoid()

	assert.Equal(t, s.Value(0.0), 0.0)
	assert.InEpsilon(t, s.Value(0.25), 0.105, 0.01)
	assert.InEpsilon(t, s.Value(0.375), 0.26, 0.01)
	assert.Equal(t, s.Value(0.5), 0.5)
	assert.InEpsilon(t, s.Value(0.625), 0.74, 0.01)
	assert.InEpsilon(t, s.Value(0.75), 0.895, 0.01)
	assert.Equal(t, s.Value(1.0), 1.0)
}
