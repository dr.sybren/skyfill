package sky

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"image"
	"image/color"

	"github.com/sirupsen/logrus"
)

const (
	// Squared 16-bit colour difference that's still considered to be 'close
	// enough' to the top-left pixel colour to be 'sky gap'. This non-exact
	// matching is done so that you can do colour grading (and things like
	// 'film-like' looks that might not uniformly map colours) on a drone
	// panorama, and run Skyfill afterwards.

	// This value was determined experimentally based on just one photo where old
	// behaviour of matching exactly didn't work.
	sqMaxColourDiff16bit = 6000000
)

// FindSkyHeight analyses the image to find the height of area with blank holes,
// and a suitable height for blending.
func FindSkyHeight(
	sourceImage image.Image,
	blendHeightFactor float64,
	expandFactor float64,
) Size {
	bounds := sourceImage.Bounds()
	var seenAlpha, seenTopLeft bool

	topLeftColour := sourceImage.At(bounds.Min.X, bounds.Min.Y)
	logrus.WithField("top_left", topLeftColour).Info("finding sky gap height")

	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		seenAlpha = false
		seenTopLeft = false
		for x := bounds.Min.X; x < bounds.Max.X; x++ {
			colour := sourceImage.At(x, y)
			_, _, _, a := colour.RGBA()
			if a < 65535 {
				seenAlpha = true
				break
			}
			colourDiff := squareColourDifference(colour, topLeftColour)
			if colourDiff <= sqMaxColourDiff16bit {
				seenTopLeft = true
				break
			}
		}

		if !seenAlpha && !seenTopLeft {
			logrus.WithFields(logrus.Fields{
				"factor":      blendHeightFactor,
				"seenAlpha":   seenAlpha,
				"seenTopLeft": seenTopLeft,
			}).Debug("found sky")
			skySize := NewSize(bounds, y, blendHeightFactor)
			skySize.expand(expandFactor)
			return skySize
		}
	}

	logrus.Warning("unable to detect sky")
	return NewSize(bounds, 0.0, 0.0)
}

func squareColourDifference(a, b color.Color) int64 {
	ar, ag, ab, _ := a.RGBA()
	br, bg, bb, _ := b.RGBA()

	diffr := int64(ar) - int64(br)
	diffg := int64(ag) - int64(bg)
	diffb := int64(ab) - int64(bb)

	return diffr*diffr + diffg*diffg + diffb*diffb
}
