package sky

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"image"
	"image/color"
	"image/draw"
)

// Size contains size information about the sky.
// All heights are expressed in pixels from the top row.
type Size struct {
	inputImageBounds image.Rectangle

	PureSkyHeight int
	BlendHeight   int
}

// NewSize returns a Size with blend height factor accounted for.
func NewSize(inputImageBounds image.Rectangle, skyHeight int, blendHeightFactor float64) Size {
	size := Size{inputImageBounds, skyHeight, 0.0}
	size.determineBlendHeight(blendHeightFactor)
	return size
}

func (s *Size) expand(factor float64) {
	lowerBy := int(factor * float64(s.PureSkyHeight))
	s.PureSkyHeight += lowerBy
}

func (s *Size) determineBlendHeight(blendHeightFactor float64) {
	s.BlendHeight = int(blendHeightFactor * float64(s.PureSkyHeight))
}

// IsEmpty returns true iff the sky height is zero.
func (s *Size) IsEmpty() bool {
	return s.PureSkyHeight == 0
}

// TotalHeight returns the total height of the sky (pure sky + blend area).
func (s *Size) TotalHeight() int {
	return s.PureSkyHeight + s.BlendHeight
}

// TotalBounds returns the image rectangle covered by the sky, including blend area.
func (s *Size) TotalBounds() image.Rectangle {
	return image.Rect(
		s.inputImageBounds.Min.X, s.inputImageBounds.Min.Y,
		s.inputImageBounds.Max.X, s.TotalHeight())
}

// PureSkyBounds returns the image rectangle covered by only the pure sky, no blend area.
func (s *Size) PureSkyBounds() image.Rectangle {
	return image.Rect(
		s.inputImageBounds.Min.X, s.inputImageBounds.Min.Y,
		s.inputImageBounds.Max.X, s.PureSkyHeight)
}

// DrawBounds draws black lines to demark the pure sky and the blend area.
func (s *Size) DrawBounds(sourceImage draw.Image) {
	color := color.NRGBA{0, 0, 0, 255}
	drawLine(sourceImage, s.PureSkyHeight, s.inputImageBounds.Dx(), color)
	drawLine(sourceImage, s.TotalHeight(), s.inputImageBounds.Dx(), color)
}

func drawLine(img draw.Image, y, width int, lineColor color.Color) {
	for x := 0; x < width; x++ {
		img.Set(x, y, lineColor)
	}
}
